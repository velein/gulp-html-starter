const fileInclude = require("gulp-file-include");
const { src, dest } = require("gulp");
const markupConfig = require("../config/markup.config");

module.exports = markup = () => {
    return src("src/markup/page/index.html")
        .pipe(
            fileInclude({
                prefix: "@@",
                basepath: "src/markup/modules",
                indent: true,
            })
        )
        .pipe(dest("public"));
};
