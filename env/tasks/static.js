const browserSync = require("browser-sync");
const { src, dest } = require("gulp");

module.exports = static = () => {
    return src("static/**/*").pipe(dest("public/"));
};
