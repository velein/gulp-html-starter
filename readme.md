# 🌀 Gulp HTML Starter

Simple bolerplate for markup (HTML, JS & CSS) projects.

### 🖇 Preparation

1. **Install NodeJS**

[https://nodejs.org/en/](https://nodejs.org/en/) - preferably LTS version

To see if you already have Node.js and npm installed and check the installed version, open the terminal and run the following commands:

```
node -v
npm -v
```

### 🚀 Quick start

1. Open project folder in your terminal
2. First, you have to install necessary dependencies, you can do that with following command

```
npm install
```

3. Now you can run the project via

```
npm start
```

4. Your site is now running at `http://localhost:3000`!

    Note: The BrowserSync's UI is disabled by default. If you would like to use it just go to `env/config/browserSync.config.js` and set ui key to `true`.

### 🏗 Folder structure

#### Images, SVGs & other static files

These files should be placed in `static` directory.

The static directory will be copied automatically to public directory.

If you want an image to be in `public/assets/images/example-image.jpg` then simply place it in `static/assets/images/example-image.jpg`

#### SCSS

Source files are located in `src/scss/` and will be transpiled and compiled to `public/assets/css/style.css`.

Vendor/third-party files should be placed in static directory, e.g `static/assets/css/vendor/normalize.css`

#### JS

Source files are located in `src/js/` and will be transipled and compiled to `public/assets/js/bundle.js`.

Vendor/third-party files should be placed in `static` directory, e.g `static/assets/js/vendor/slick.min.js`

#### Markup Files (HTML)

Source files are located in `src/markup/`. Files from `src/markup/page/*.html` will be compiled to `public` directory.
